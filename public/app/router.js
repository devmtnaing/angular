sephora.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($httpProvider, $stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

  $urlRouterProvider.otherwise("/products");


  $stateProvider

  // Products
  .state('products', {
      url: '/products',
      templateUrl: 'views/products/index.html',
      controller: 'ProductController',
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
              files: [
                  'app/controllers/ProductController.js',
                  'app/factories/ProductFactory.js',
                  'app/factories/DataFactory.js',
                  'app/services/DataService.js',
                  'app/services/ProductService.js'
              ]
          });
        }]
      }
  })

  .state('detail', {
      url: '/products/detail/:id',
      templateUrl: 'views/products/detail.html',
      controller: 'ProductController',
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
              files: [
                  'app/controllers/ProductController.js',
                  'app/factories/ProductFactory.js',
                  'app/factories/DataFactory.js',
                  'app/services/DataService.js',
                  'app/services/ProductService.js'
              ]
          });
        }]
      }
  })

  .state('product_by_category', {
      url: '/products/category/:category',
      templateUrl: 'views/products/index.html',
      controller: 'ProductController',
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
              files: [
                  'app/controllers/ProductController.js',
                  'app/factories/ProductFactory.js',
                  'app/factories/DataFactory.js',
                  'app/services/DataService.js',
                  'app/services/ProductService.js'
              ]
          });
        }]
      }
  });

  

}]);
