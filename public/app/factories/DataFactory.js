angular.module("Sephora").factory("DataFactory", function($http, BaseAPI) {

  var Data = [];

  Data.getAll = function(){
    return $http.get('./uploads/data.json');
  }

  return Data; 
});